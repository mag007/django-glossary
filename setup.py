from distutils.core import setup

setup(
    name='django-glossary',
    version='0.1.0',
    author='Magnetic Creative',
    author_email='dev@mag.cr',
    packages=['django_glossary'],
    license='LICENSE.txt',
    description='Glossary app.',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.1.1",
    ],
)