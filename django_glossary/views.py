from django.views.generic import TemplateView
from models import Term
import string

class IndexView(TemplateView):
    template_name = 'glossary/index.html'
    _terms = None

    def get_context_data(self, **kwargs):
        return {'glossary': self.terms(), 'alphabet':sorted(self.terms().keys())}

    def render_to_response(self, context, **response_kwargs):
        if 'app_config' in getattr(self.request, '_feincms_extra_context', {}):
            return self.get_template_names(), context

        return super(IndexView, self).render_to_response(context, **response_kwargs)

    def terms(self):
        if not self._terms:
            self._terms = {}
            for l in string.lowercase[:26]:
                self._terms[l] = Term.objects.filter(term__istartswith=l).values('term', 'definition')

        return self._terms