from django.db import models

# Create your models here.

class Term(models.Model):
    term = models.CharField(max_length=200)
    definition = models.TextField()
    sortable_name = models.CharField(max_length=200, blank=True)

    def __unicode__(self):
        return u'%s' % (self.term)